import requests
import json
import logging
import django
import os
from datetime import datetime
import os
import sys

logger = logging.getLogger(__name__)

def setup_script():
    # config log
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hacking_for_humanity_1120.settings")
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(0, parent_dir)
    fmt = '%(asctime)s : %(levelname)s : %(message)s'
    datefmt = '%m%d %H:%M:%S'
    logging.basicConfig(
        level=logging.INFO,
        format=fmt,
        datefmt=datefmt,
    )
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    os.sys.path.insert(0, parent_dir)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hacking_for_humanity_1120.settings")
    django.setup()


if __name__ == '__main__':

    setup_script()

    from safe_leisure.models import Local, POI
    from django.contrib.gis.geos import Point
    
    for local_obj in Local.objects.all():
        new_poi = POI()
        new_poi.aforo_terraza = local_obj.aforo_terraza
        new_poi.coordenada_x = local_obj.coordenada_x
        new_poi.coordenada_y = local_obj.coordenada_y
        new_poi.longitud = local_obj.longitud
        new_poi.latitud = local_obj.latitud
        new_poi.desc_ubicacion_terraza = local_obj.desc_ubicacion_terraza
        new_poi.direccion_local = local_obj.direccion_local
        new_poi.horario_terraza_fin = local_obj.horario_terraza_fin
        new_poi.horario_terraza_ini = local_obj.horario_terraza_ini
        new_poi.id_local = local_obj.id_local
        new_poi.id_terraza = local_obj.id_terraza
        new_poi.is_terraza = local_obj.is_terraza
        new_poi.nombre = local_obj.nombre
        new_poi.tipo = local_obj.tipo
        new_poi.geom = Point(local_obj.latitud, local_obj.longitud)
        new_poi.save()
        
        print(new_poi.nombre)

