from django.apps import AppConfig


class SafeLeisureConfig(AppConfig):
    name = 'safe_leisure'
